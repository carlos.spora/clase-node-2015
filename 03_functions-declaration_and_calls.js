/**
 * Created by Carlos Martinez on 11/10/2015.
 */

console.log([
    "************************************",
    "* Functions: declaration and calls *",
    "************************************"
].join("\n"));

/**
 * FUNCTIONS: declaration y calls
 */
var f1 = function(){
    return "Hello";
};
function f2(){
    return "World";
}
console.log(f1(), f2());

//Static parameters
function f3(a){
    console.log(a);
}
f3();
f3("HEYAAA!");
f3("Hello", "Piriii");

//Variable parameters (arity)
function f4(){
    for(var i = 0; i < arguments.length; i++)
        console.log("Arg " + i + ": " + arguments[i]);
}
f4(2, "Hello", {b:12});

//Returning functions as result
function multiplyBy(x){
    return function(y){return x * y};
}
var triply = multiplyBy(3);
console.log(triply(7));

//Calling during declaration
(function(str){
    console.log(str);
}("Calling during declaration"));
