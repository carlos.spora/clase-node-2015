/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "*******************",
    "* Object oriented *",
    "*******************"
].join("\n"));


/**
 * Object oriented
 */

// Point constructor.
function Point(x,y) {
    this.x = x;
    this.y = y;
}

// Segment constructor.
function Segment(p1,p2) {
    this.p1 = p1;
    this.p2 = p2;

    //this.length = function(){return distance(this.p1, this.p2)};
}

// Auxiliar. Function that computes the distance between two Point objects.
function distance(a,b) {
    var dx = b.x-a.x, dy = b.y-a.y;
    return Math.sqrt(dx*dx + dy*dy);
}

// length method shared for all Segment objects
Segment.prototype.length = function() {
    return distance(this.p1, this.p2)
}

var p1 = new Point(10,0);
var p2 = new Point(5,5);
var p3 = new Point(3,6);
var se1 = new Segment(p1,p2);
var se2 = new Segment(p2,p3);

console.log("Length of segment 'se1' is: " + se1.length());
console.log("Length of segment 'se2' is: " + se2.length());