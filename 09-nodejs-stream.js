/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "******************",
    "* NODEJS: stream *",
    "******************"
].join("\n"));

var st = require('./Circle.js')

console.log("Radius of the circle: ")

// Needed for initiating the reads from stdin.
process.stdin.resume();
// Needed for reading strings instead of �Buffers�.
process.stdin.setEncoding("utf8");

process.stdin.on("data", function(str) {
    var rd = str.slice(0,str.length-2); //remove endline.
    console.log("Circumference for radius " + rd + " is " + st.circumference(rd) + "\n")
    console.log("Radius of the circle: ")
});