/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "***************",
    "* NODEJS: net *",
    "***************"
].join("\n"));

var net = require('net');

// SERVER
net.createServer(
    function(c) { //'connection' listener
        console.log('Server: client connected.');

        c.on('end', function() {
            console.log('Server: client disconnected.');
        });

        c.write('Hello '); // Send "Hello" to the client.
        c.pipe(c); // Echo what is received and end
    }
).listen(9000, function() { //'listening' listener
    console.log('Server listening at port 9000');
});

//CLIENT
setTimeout(function(){
    var client = net.connect({port: 9000}, function() { //'connect' listener
        console.log('Client: connected to server');
        client.write('world!');// This will be echoed by the server.
    });

    client.on('data', function(data) {
        console.log(data.toString());// Write the received data to stdout.
        client.end();
    });

    client.on('end', function() {
        console.log('Client: disconnected from server');
    });
}, 2000);
