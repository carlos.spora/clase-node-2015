/**
 * Created by Carlos on 11/10/2015.
 */

var Circle = {};

(function(Circle){
    Circle.circumference = function(rd){
        return 2 * Math.PI * rd;
    };
}(Circle));

module.exports = Circle;