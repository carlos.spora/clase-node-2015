/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "********************",
    "* NODEJS: promises *",
    "********************"
].join("\n"));

/**
 * 3 STATES
 * -onFulfilled
 * -onRejected
 * -onProgress
 */

//IMPORTS
var Promise = require("bluebird");
var fs = require("fs");


var readFileAsync = Promise.promisify(fs.readFile); //builds a variant of asynchronous fs.readFile() that returns a promise
var myFile = readFileAsync("./package.json", "utf8");// Obtain the promise.

function printFile (contents) {
    console.log(contents);
    return contents;
}

function showLength(contents) {
    console.log(contents.length);
    return contents;
}

function showErrors (err) {
    console.log( "Errors reading file...");
    console.log(err);
}

// Use the promise, chaining multiple then()s, if needed.
myFile.then(printFile).then(showLength, showErrors);