/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "********************************",
    "* Object oriented: inheritance *",
    "********************************"
].join("\n"));

/**
 * Object oriented - Inheritance
 */
function RegularPolygon(nl,ll) {
    this.sides = nl;
    this.sideLength = ll;
}

function Square(ll) {
    this.sides = 4;
    this.sideLength = ll
}

// perimeter() methid for RegularPolygon.
RegularPolygon.prototype.perimeter = function() {
    return this.sides * this.sideLength;
};

// Inheritance
Square.prototype = Object.create(RegularPolygon.prototype);
Square.prototype.constructor = Square;

// La clase Cuadrado tendr�, adem�s, el m�todo superficie().
Square.prototype.surface = function() {
    return this.sideLength * this.sideLength;
}

var c = new Square(6)

console.log("Square perimeter is: " + c.perimeter());
console.log("and its superficie is: " + c.surface());