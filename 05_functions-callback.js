/**
 * Created by Carlos Martinez on 11/10/2015.
 */

console.log([
    "***********************",
    "* Functions: callback *",
    "***********************"
].join("\n"));

/**
 * Functions: callback
 */


function f1(cb1, cb2){
    cb1();
    cb1();
    cb2();
    cb1();
}
f1(function(){console.log("cb1")}, function(){console.log("cb2")});

//************************************************
console.log();

f1 = function(s, callback){
    setTimeout(function(){
        callback("I've waited for " + s + " seconds.")
    }, s * 1000)
};

console.log("Before!")
f1(3, function(texto){
    console.log(texto);
    console.log("After!")
});
console.log("Also after!");

