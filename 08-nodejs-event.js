/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "******************",
    "* NODEJS: events *",
    "******************"
].join("\n"));

var events = require('events');

var emitter = new events.EventEmitter();
var e1 = "print";
var e2 = "read";
var num1 = 0;
var num2 = 0;

// Listener functions are registered in the event emitter.
emitter.on(e1, function() {
    console.log("Event " + e1 + " has " + "happened " + ++num1 + " times.")
});
emitter.on(e2, function() {
    console.log("Event " + e2 + " has " + "happened " + ++num2 + " times.")
});

// There might be more than one listener for the same event.
emitter.on(e1, function(){
    console.log("Something has been printed!!");
});

// Generate the events periodically...
setInterval( function() {emitter.emit(e1)}, 2000 ); // First event generated every 2s
setInterval( function() {emitter.emit(e2)}, 3000 ); // Second event generated every 3s