/**
 * Created by Carlos Martinez on 11/10/2015.
 */

console.log([
    "************************",
    "* VARIABLES: operators *",
    "************************"
].join("\n"));

/**
 * OPERATORS
 */
console.log("Primitive values");
var a = '7';
console.log("a == 7 -> ", a == 7);
console.log("a === 7 -> ", a === 7);
console.log("a + 23 -> ", a + 23);
console.log("a + '67' -> ", a + '67');
console.log("a * 5 -> ", a * 5);
console.log("parseInt(a) + 8 -> ", parseInt(a) + 8);

console.log("\nCompound values");

var a = {b: 4};
var b = {b: 4};
c = a;

console.log("a == b -> ", a == b);
console.log("a === b -> ", a === b);
console.log("a == c -> ", a == c);
console.log("a === c -> ", a === c);

