/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "**************************",
    "* NODEJS: promises defer *",
    "**************************"
].join("\n"));

/**
 * 3 WAYS
 * -promisify
 * -defer() + callback
 * -defer()
 */

//IMPORTS
var Promise = require("bluebird");
var fs = require("fs");

//PROMISIFY
var readFileAsync1 = Promise.promisify(fs.readFile);
readFileAsync1("package.json", "utf8").then(function(txt){console.log(txt)}, function(err){console.error(err)});

//defer() + callback
var readFileAsync2 = function(filename){
    var resolver = Promise.defer();
    fs.readFile(filename, "utf8", resolver.callback);
    return resolver.promise;
}
readFileAsync2("package.json").then(function(txt){console.log(txt)}, function(err){console.error(err)});

//defer()
var readFileAsync3 = function(filename){
    var resolver = Promise.defer();
    fs.readFile(filename, "utf8", function(err, res){
        if(err) resolver.reject(new Error(err));
        else resolver.resolve(res);
    });
    return resolver.promise;
};
readFileAsync3("package.json").then(function(txt){console.log(txt)}, function(err){console.error(err)});