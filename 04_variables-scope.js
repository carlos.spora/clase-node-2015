/**
 * Created by Carlos Martinez on 11/10/2015.
 */

console.log([
    "********************",
    "* Variables: scope *",
    "********************"
].join("\n"));

/**
 * VARIABLES: scope
 */

//'use strict';

var global = "global"; //Global
global2 = "global2"; //Global

console.log(typeof global, typeof global2, typeof global3, typeof local_f1);

(function f1(){
    console.log(typeof global, typeof global2, typeof global3, typeof local_f1);

    var local_f1 = "localf1"; //Local
    global3 = "global3"; //Global

    console.log(typeof global, typeof global2, typeof global3, typeof local_f1);
}());

console.log(typeof global, typeof global2, typeof global3, typeof local_f1);