/**
 * Created by Carlos on 11/10/2015.
 */

console.log([
    "****************",
    "* NODEJS: http *",
    "****************"
].join("\n"));

var http = require('http');

//SERVER
http.createServer(function (req, res) {
    console.log("Request: " + req.url);
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World');
}).listen(8000, "127.0.0.1");
console.log('Server running at http://127.0.0.1:8000/');

//CLIENT
setTimeout(function(){
    var data = "";
    http.get("http://127.0.0.1:8000", function(res) {
        var data = "";
        res.on("data", function(chunk){
            data += chunk;
        });
        res.on("end",function(){
            console.log("Got response: " + res.statusCode);
            console.log("Body: " + data);
        });
    }).on('error', function(e) {
        console.log("Got error: " + e.message);
    });
},2000);