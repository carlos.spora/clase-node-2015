/**
 * Created by Carlos Martinez on 11/10/2015.
 */

console.log([
    "************************************",
    "* VARIABLES: declaration and types *",
    "************************************"
].join("\n"));

/**
 * VARIABLES
 */
var a; //Without type
const b = 10; //Constant

/**
 * PRIMITIVE VALUES
 * - Uses values
 * - Immutable
 */
console.log("\nPRIMITIVE VALUES")
a = null; //no object
console.log(a + " -> " + typeof a);
a = undefined; //variable no initialized
console.log(a + " -> " + typeof a);
a = true; //Boolean
console.log(a + " -> " + typeof a);
a = 15; //Number
console.log(a + " -> " + typeof a);
a = "Hello world"; //String
console.log(a + " -> " + typeof a);

/**
 * COMPOUND VALUES (Objects)
 * - Use referencies
 * - Mutable
 */
console.log("\nCOMPOUND VALUES");
var a = {a: 1, b: "Hello"}; //Object
console.log(a + " -> " + typeof a);
var a = [1, 3, 7]; //Array
console.log(a + " -> " + typeof a);
var a = /' '*/g; //Regex
console.log(a + " -> " + typeof a);
var a = function(){}; //Function
console.log(a + " -> " + typeof a);
